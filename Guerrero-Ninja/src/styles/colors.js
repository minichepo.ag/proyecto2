const colors = {
    primary: "#F56060",
    dark: "#000",
    
    //Fonts
    fontLight: "#fff",
    //background
    bgLight: "#fff",
    bgDark: "#16222b",
    bgBar: "#FF6E1A", //naranja
    bgBar2: "#FF6E1A", //naranja


    rojo: "#F56060"
};

export default colors;