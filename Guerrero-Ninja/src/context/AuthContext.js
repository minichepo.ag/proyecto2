import {createContext} from "react";

const AuthContext = createContext({
    auth: undefined,
    login: () => null, 
    logout: () => null, //Mandar al panel de usuario logeado
});

export default AuthContext; //Exportar el AuthContext