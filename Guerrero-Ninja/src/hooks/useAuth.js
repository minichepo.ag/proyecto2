 import {useContext} from "react";
 import AuthContext, {} from "../context/AuthContext";

 export default () => useContext(AuthContext);
 //Retorna la funcion que ejecuta el contexto