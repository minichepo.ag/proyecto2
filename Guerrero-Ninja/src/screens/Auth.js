import React, {useState} from 'react'
import { StyleSheet, View, Text, Image, KeyboardAvoidingView, Platform } from 'react-native'
import RegisterForm from "../components/Auth/RegisterForm"
import LoginForm from "../components/Auth/LoginForm"
import logo from "../../assets/logo-login.png"
import {layoutStyle} from "../styles";



export default function Auth() {
    const [showLogin, setshowLogin] = useState(true);

    const changeForm = () => setshowLogin (!showLogin);
    return (
        <View style = {layoutStyle.container}>
            <Image style={styles.logo} source={logo}/>
            
                {showLogin ? (
                    <LoginForm changeForm={changeForm}/>
                ) : (<RegisterForm changeForm = {changeForm}/>
                    
                )}
        </View>
    )
}

const styles = StyleSheet.create({
    logo: {
        width: "100%",
        height: 150,
        resizeMode: "contain",
        marginBottom:50, 
        
    },
});

//HOLA