import React from 'react'
import { Alert } from 'react-native'
import {List} from "react-native-paper";
import {useNavigation} from "@react-navigation/native";
import useAuth from "../../hooks/useAuth"

export default function Menu() {
    const navigation= useNavigation();
    const {logout} = useAuth();


    const logoutAccount = () =>{
        
    Alert.alert (
        'Cerrar sesion', 
        '¿Estas seguro que quieres salir de tu cuenta?', 
        [
            {
                text: "NO",
            },
            {
                text: "SI",
                onPress: logout,
            },
        ],

        {cancelable: false}
        
    );
}

    
    return (
        <>
        <List.Section>
            <List.Subheader>MI CUENTA</List.Subheader>
            <List.Item
                title="Cambiar el nombre" //Texto que se muestra
                description="Cambia el nombre de tu cuenta" //Subtexto
                left={(props)=><List.Icon{...props} icon="face"/>}
                onPress={() =>navigation.navigate("change-name")}
            />

            <List.Item
                title="Cambiar email" //Texto que se muestra
                description="Cambia el email de tu cuenta" //Subtexto
                left={(props)=><List.Icon{...props} icon="email"/>}
                onPress={() => navigation.navigate("change-email")}
            />

            <List.Item
                title="Cambiar username" //Texto que se muestra
                description="Cambia el nombre de usuario de tu cuenta" //Subtexto
                left={(props)=><List.Icon{...props} icon="at"/>}
                onPress={() => navigation.navigate("change-username")}
            />

            <List.Item
                title="Cambiar contraseña" //Texto que se muestra
                description="Cambia la contraseña de tu cuenta" //Subtexto
                left={(props)=><List.Icon{...props} icon="key"/>}
                onPress={() => navigation.navigate("change-password")}
            />

            <List.Item
                title="Mis direcciones" //Texto que se muestra
                description="Administra tus direcciones" //Subtexto
                left={(props)=><List.Icon{...props} icon="map"/>}
                onPress={() => navigation.navigate("addresses")}
            />
        </List.Section>

        <List.Section>
            <List.Subheader>APP</List.Subheader>
            <List.Item
                title="Pedidos" //Texto que se muestra
                description="Listados de todos los pedidos" //Subtexto
                left={(props)=><List.Icon{...props} icon="clipboard-list"/>}
                onPress={() => console.log("Ir a a mis pedidos")}
            />

            <List.Item
                title="Lista de deseos" //Texto que se muestra
                description="Listado de productos que quieres comprar" //Subtexto
                left={(props)=><List.Icon{...props} icon="heart"/>}
                onPress={() => navigation.navigate("favorites")}
            />

            <List.Item
                title="Cerrar sesión" //Texto que se muestra
                description="Cierra esta sesión" //Subtexto
                left={(props)=><List.Icon{...props} icon="logout"/>}
                onPress={logoutAccount}
            />
        </List.Section>

        </>
    );
}
