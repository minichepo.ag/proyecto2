import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text } from "react-native";
import ListProduct from "./ListProduct";
import { getLastProuctsApi } from "../../api/product";
 
export default function NewProducts() {
    const [products, setProducts] = useState(null);

    useEffect(() => {
        (async () => {
          const response = await getLastProuctsApi();
          setProducts(response);
        })();
      }, []);
       
      return (
        <View style={styles.container}>
          <Text style={styles.title}>Nuestros productos</Text>
          {products && <ListProduct products={products} />}
        </View>
      );
    }
    
    const styles = StyleSheet.create({
      container: {
        padding: 10,
        marginTop: 15,
      },
      title: {
        fontWeight: "bold",
        fontSize: 21,
        marginBottom: 10,
      },
    });