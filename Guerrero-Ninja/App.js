import React, {useState, useEffect, useMemo} from "react";
import { View, Text, Button } from 'react-native';
import {Provider as PaperProvider} from "react-native-paper"
import jwtDecode from "jwt-decode";
import AppNavigation from "./src/navigation/AppNavigation";
import AuthScreen from "./src/screens/Auth";
import AuthContext from "./src/context/AuthContext"; //Exportar el AuthContext
import {setTokenApi, getTokenApi, removeTokenApi} from "./src/api/token";
import { NavigationContainer } from "@react-navigation/native";



export default function App() {
  const [auth, setAuth] = useState(undefined);

  useEffect(() => {
       (async ()=>{
          const token= await getTokenApi();
          if(token){
            setAuth({
              token,
              idUser: jwtDecode(token).id
            });
          }else{
            setAuth(null);
          }
       }) ()

  }, []);

  const login = (user) => {  //Setear el usuario de LoginForm
      setTokenApi(user.jwt) //Se pasa el token del objeto
      setAuth({
        token: user.jwt,
        idUser: user.user._id
      });
  }

  const logout = () => {
    if(auth){
        removeTokenApi();
      setAuth(null);
    }
  }

  const authData = useMemo(
    () => ({
      auth,
      login,
      logout,
    }),
    [auth] //comprueba si se realiza un cambio
  );

  if(auth === undefined) return null;
  //Si el usuario esta logeado devuelve el return

  return (
    <AuthContext.Provider value ={authData} >  
      <PaperProvider>
     {auth?( 
       <AppNavigation/>
    ):( 
      <AuthScreen/>
     )}
    </PaperProvider>
    </AuthContext.Provider>
  );
}


